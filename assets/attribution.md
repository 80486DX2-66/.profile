# Attribution

I own no asset located in this directory. All the rights to them belong to their rightful owners.

| File name | Attribution |
| :-- | :-- |
| `cpu.png` | Modified https://theretroweb.com/chip/image/img-8370-6431d434caac5034866254.jpg |
